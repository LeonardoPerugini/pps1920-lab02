package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class testMain {

  import Main._
  import Shape._
  import Option._

  @Test def testParity(): Unit ={
    assertEquals("odd", parity(3))
    assertEquals("even", parity(4))
    assertEquals("odd", printParity(3))
    assertEquals("even", printParity(4))
  }

  @Test def testNeg(): Unit ={
    assertEquals(false, neg(_=="")(""))
    assertEquals(true, neg(_=="")("something"))
    assertEquals(false, neg2(_=="")(""))
    assertEquals(true, neg2(_=="")("something"))
    assertEquals(false, gNeg[Int](_ == 0)(0))
    assertEquals(true, gNeg[Int](_ == 0)(10))
  }

  @Test def testCurrying(): Unit={
    assertEquals(true, valCurried(10)(20)(30))
    assertEquals(false, valCurried(20)(19)(30))
    assertEquals(true, valNotCurried(10, 20, 30))
    assertEquals(false, valNotCurried(20, 19, 30))
    assertEquals(true, defCurried(10)(20)(30))
    assertEquals(false, defCurried(10)(9)(30))
    assertEquals(true, defNotCurried(10, 20, 30))
    assertEquals(false, defNotCurried(20, 19, 30))
  }

  @Test def testCompose(): Unit ={
    assertEquals(9, compose(_-1, _*2)(5))
    assertEquals(13, compose(_-2, _*3)(5))
  }

  @Test def testFibonacci(): Unit = {
    assertEquals(3, fibonacci(4))
    assertEquals(8, fibonacci(6))
  }

  @Test def testShape(): Unit ={
    assertEquals(200, perimeter(Square(50)))
    assertEquals(2500, area(Square(50)))
    assertEquals(120, perimeter(Rectangle(50, 10)))
    assertEquals(500, area(Rectangle(50, 10)))
    assertEquals((2*Math.PI), perimeter(Circle(1)))
    assertEquals(Math.PI, area(Circle(1)))
  }

  @Test def testOption(): Unit ={
    testEmpty()
    testGetorElse()
    testFlatMap()
    testFilter()
    testMap()
    testMap2()
  }

  @Test def testEmpty(): Unit ={
    assertEquals(true, isEmpty(None()))
    assertEquals(false, isEmpty(Some(1)))
  }

  @Test def testGetorElse(): Unit ={
    assertEquals(1, getOrElse(Some(1), 2))
    assertEquals(2, getOrElse(None(), 2))
  }

  @Test def testFlatMap(): Unit ={
    assertEquals(Some(6), flatMap(Some(5))(i => Some(i+1)))
    val none: Option[Int] = None()
    assertEquals(None(), flatMap(none)(i => Some(i+1)))
  }

  @Test def testFilter(): Unit ={
    assertEquals(None(), filter(Some(8))(a => isEmpty(a)))
    assertEquals(Some(8), filter(Some(8))(a => !isEmpty(a)))
  }

  @Test def testMap(): Unit ={
    assertEquals(Some(true), map(Some(1))(a => !isEmpty(a)))
    assertEquals(None(), map(None[Int]())(a => isEmpty(a)))
  }

  @Test def testMap2(): Unit ={
    assertEquals(Some(1), map2(Some(1), Some(2))((i,j) => i))
    assertEquals(None(), map2(None(), Some(2))((i, j) => i))
  }


}
